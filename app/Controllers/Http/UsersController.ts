import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UserValidator from 'App/Validators/UserValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import Database from '@ioc:Adonis/Lucid/Database'
import Mail from '@ioc:Adonis/Addons/Mail'

export default class UsersController {

      /**
  * @swagger
  * /api/v1/register:
  *   post:
  *     tags:
  *       - Authentication  
  *     requestBody:    
  *       required: true  
  *       content:    
  *        application/x-www-form-urlencoded:
  *          schema:
  *            $ref: '#definitions/User'
  *     responses:
  *       '201':
  *         description: user created, verify opt in eamil
  *       '422': 
  *         description: request invalid
  */
    public async register({ request, response }: HttpContextContract){
        try {
            await request.validate(UserValidator)
            
            const name = request.input('name')
            const email = request.input('email')
            const password = request.input('password')

            const user =  await User.create({ name, email, password })
            const otpCode = Math.floor(100000 + Math.random() * 900000)
            await Database.table('otp_codes').insert({
                otp_code: otpCode,
                user_id: user.id
            })

            await Mail.send((message) => {
                message
                  .from('owner@example.com')
                  .to(email)
                  .subject('yours OTP!')
                  .htmlView('emails/otp_verification', { otpCode })
            })

           return response.status(201).json({
                status: 'ok',
                message: 'Registerr success, please verify your otp!',
            })
        } catch (error) {
            return response.unprocessableEntity({
                message: error.messages
            })
        }

    }
      /**
  * @swagger
  * /api/v1/confirmation-otp:
  *   post:
  *     tags:
  *       - Authentication
  *     requestBody:    
  *       required: true  
  *       content:    
  *        application/x-www-form-urlencoded:
  *          schema:
  *            $ref: '#definitions/User'
  *     responses:
  *       '201':
  *         description: success confirmation OTP
  *       '422': 
  *         description: request invalid
  */
    public async otpConfirmation({ request, response }: HttpContextContract){
        let otpCode = request.input('otp_code')
        let email = request.input('email')

        let user = await User.findBy('email', email)
        let otpChceck = await Database.query().from('otp_codes').where('otp_code', otpCode).first()
        if(user?.id == otpChceck.user_id){
            const newuser = await User.findOrFail(user)
            newuser.isVerified = true
            await newuser.save()

            return response.status(200).json({
                status: 200,
                message: 'success confirmation OTP'
            })
        }
    }


         /**
  * @swagger
  * /api/v1/login:
  *   post:
  *     tags:
  *       - Authentication  
  *     requestBody:    
  *       required: true  
  *       content:    
  *        application/x-www-form-urlencoded:
  *          schema:
  *            $ref: '#definitions/User'
  *     responses:
  *       '201':
  *         description: success Login
  *       '422': 
  *         description: request invalid
  */
    public async login({ request, response, auth }: HttpContextContract){
        const UserSchema = schema.create({
            email: schema.string(),
            password: schema.string()
        })

        const email = request.input('email')
        const password = request.input('password')

        try {
            await request.validate({ schema: UserSchema })
            const token = await auth.use('api').attempt(email, password)
            return response.status(200).json({
                status: 'ok',
                messgge: token
            })
        } catch (error) {
            if(error.guard){
                return response.badRequest({
                    message: 'Login error',
                    error: error.message
                })
            }else{
                return response.badRequest({
                    message: 'Login error',
                    error: error.messages
                })
            }
        }
    }
}
