import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'
import RegisVenueValidator from 'App/Validators/RegisVenueValidator'

export default class VenuesController {
    /**
  * @swagger
  * /api/v1/venues:
  *   get:
  *     tags:
  *       - get all Venue
  *     responses:
  *       200:
  *         description: Send index message
  *         example:
  *           message: managed to get data
  */
  public async index ({ response }: HttpContextContract) {
    const venue = await Venue.query().preload('fields')
    return response.status(200).json({
      status: 'ok',
      message: 'managed to get data',
      data: venue
    })
  }


        /**
  * @swagger
  * /api/v1/venues:
  *   post:
  *     tags:
  *       - Create Venues  
  *     requestBody:    
  *       required: true  
  *       content:    
  *        application/x-www-form-urlencoded:
  *          schema:
  *            $ref: '#definitions/User'
  *     responses:
  *       '201':
  *         description: success created venues
  *       '422': 
  *         description: request invalid
  */
  public async store ({ request, response }: HttpContextContract) {
      const venueSchema = schema.create({
        name: schema.string({ trim: true }),
        phone: schema.string({ escape: true }),
        address: schema.string({ escape: true }),
      })
      await request.validate({ schema: venueSchema })

      const name = request.input('name')
      const phone = request.input('phone')
      const address = request.input('address')

      await Venue.create({ name, phone, address })

      return response.status(201).json({
        status: 'ok',
        message: 'success created venues'
      })

  }


        /**
  * @swagger
  * /api/v1/venues/:id/field:
  *   post:
  *     tags:
  *       - Create Register Venues  
  *     requestBody:    
  *       required: true  
  *       content:    
  *        application/x-www-form-urlencoded:
  *          schema:
  *            $ref: '#definitions/Field'
  *     responses:
  *       '201':
  *         description: success created venues
  *       '422': 
  *         description: request invalid
  */
   public async registerVenues({ request, response, params }:HttpContextContract){
      request.validate(RegisVenueValidator)

      const venueId = params.id
      const venue = await Venue.findBy('id', venueId)
      
      const newField = new Field()
      newField.name = request.input('name')
      newField.type = request.input('type')
      
      await venue?.related('fields').save(newField)

      return response.status(201).json({
        status: 'ok',
        message: 'success created!'
      })

  }

         /**
  * @swagger
  * /api/v1/venues/:id/bookings:
  *   post:
  *     tags:
  *       - Booking venues
  *     requestBody:    
  *       required: true  
  *       content:    
  *        application/x-www-form-urlencoded:
  *          schema:
  *            $ref: '#definitions/Booking'
  *     responses:
  *       '201':
  *         description: craetd booking
  *       '422': 
  *         description: request invalid
  */
  public async venueBookings ({ request, response, params }: HttpContextContract) {

    const venueId = params.id
    const venue = await Field.findBy('id', venueId)

    const booking = new Booking()
    booking.fieldId = request.input('field_id')
    booking.play_date_start = request.input('play_date_start')
    booking.play_date_end = request.input('play_date_end')

    await venue?.related('bookings').save(booking)
    
    return response.status(201).json({
      message: 'craetd booking'
    })

  }   

     /**
  * @swagger
  * /api/v1/venues/:id:
  *   get:
  *     tags:
  *       - show venue
  *     responses:
  *       200:
  *         description: Send showVenue message
  *         example:
  *           message: managed to get data
  */
  public async showVenue ({ response, params }: HttpContextContract) {

    const venue = await Venue.find(params.id)
    return response.status(200).json({
      status: 'ok',
      message: 'managed to get data',
      data: venue
    })
  }

}
