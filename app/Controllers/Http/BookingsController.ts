import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'

export default class BookingsController {
 
    /**
  * @swagger
  * /api/v1/booking:
  *   get:
  *     tags:
  *       - get all booking
  *     responses:
  *       200:
  *         description: Send booking message
  *         example:
  *           message: managed to get data
  */
  public async booking ({ response }: HttpContextContract) {
    const booking = await Booking.all()
    return response.status(200).json({
      status: 'ok',
      message: 'managed to get data',
      data: booking
    })
  }

      /**
  * @swagger
  * /api/v1/booking/:id:
  *   get:
  *     tags:
  *       - get show booking
  *     parameters:
  *       - params: params
  *         description: params
  *         in: query
  *         required: false
  *         type: string
  *     responses:
  *       200:
  *         description: Send booking message
  *         example:
  *           message: managed to get data
  */
  public async show ({ response, params }: HttpContextContract) {
    const booking = await Booking.findBy('id', params.id)
    return response.status(200).json({
      status: 'ok',
      message: 'managed to get data',
      data: booking
    })
  }

  public async join ({}: HttpContextContract) {
  }

  public async unjoin ({}: HttpContextContract) {
  }

 
}
