import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

/** 
*  @swagger
*  definitions:
*    Booking:
*      type: object
*      properties:
*        id:
*          type: uint
*        play_date_start:
*          type: DateTime
*        play_date_end:
*          type: string
*      required:
*        - play_date_start
*        - play_date_end
*/
export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public play_date_start: DateTime

  @column()
  public play_date_end: string

  @column()
  public userId: number

  @column()
  public fieldId: number

  @column()
  public type: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
