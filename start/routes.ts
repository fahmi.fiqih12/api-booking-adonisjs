/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.post('api/v1/register', 'UsersController.register').as('auth.register').middleware(['auth', 'verify'])
Route.post('api/v1/confirmation-otp', 'UsersController.otpConfirmation').as('auth.otpConfirmation').middleware(['auth', 'verify'])
Route.post('api/v1/login', 'UsersController.login').as('auth.login')

Route.post('/api/v1/venues', 'VenuesController.store').as('venues.store').middleware(['auth', 'verify'])
Route.post('/api/v1/venues/:id/field', 'VenuesController.registerVenues').as('venues.regisVenues').middleware(['auth', 'verify'])
Route.get('/api/v1/venues', 'VenuesController.index').as('venues.index').middleware(['auth', 'verify'])
Route.post('/api/v1/venues/:id/bookings', 'VenuesController.venueBookings').as('venues.venueBookings').middleware(['auth', 'verify'])
Route.get('/api/v1/venues/:id', 'VenuesController.showVenue').as('venues.bookings').middleware(['auth', 'verify'])

Route.get('/api/v1/booking', 'BookingsController.booking').as('booking.booking').middleware(['auth', 'verify'])
Route.get('/api/v1/booking/:id', 'BookingsController.show').as('booking.show').middleware(['auth', 'verify'])
Route.post('/api/v1/booking/:id/join', 'BookingsController.join').as('booking.join').middleware(['auth', 'verify'])
Route.post('/api/v1/booking/:id/unjoin', 'BookingsController.unjoin').as('booking.unjoin').middleware(['auth', 'verify'])